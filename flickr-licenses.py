#! /usr/bin/python
# -*- coding: utf-8 -*-

import sys
import xml.etree.ElementTree as ET

license_map = {}

def flickr(method, **kwargs):
    from urllib import urlencode
    from urllib2 import urlopen

    kwargs["api_key"] = "cf4e02fc57240a9b07346ad26e291080"
    kwargs["method"] = method
    # TODO: sanity checks
    return ET.parse(urlopen("http://api.flickr.com/services/rest/", urlencode(kwargs)))

def fetch_licenses():
    root = flickr("flickr.photos.licenses.getInfo")
    for l in root.find("licenses"):
        license = {
            "license-name": l.attrib["name"],
            "license-url": l.attrib["url"]
            }
        license_map[int(l.attrib["id"])] = license

def lookup_photo(filename, photo_id):
    root = flickr("flickr.photos.getInfo", photo_id=photo_id)
    photo = root.find("photo")

    data = { "filename": filename }

    data.update(license_map[int(photo.attrib["license"])])

    owner = photo.find("owner")
    data["name"] = owner.attrib["realname"] or owner.attrib["username"]
    for child in photo.find("urls"):
        if child.attrib["type"] == "photopage":
            data["url"] = child.text
            break

    print u"%(filename)s\n%(url)s\n© %(name)s\n%(license-name)s (%(license-url)s)\n" % (data)

# Need to output UTF-8
import codecs
sys.stdout = codecs.getwriter('utf8')(sys.stdout)

fetch_licenses()
for l in sys.stdin:
    (filename, photo_id) = l.split(":")
    lookup_photo(filename, int(photo_id))
